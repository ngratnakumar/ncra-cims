# Generated by Django 4.1.2 on 2022-11-10 08:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='disk',
            name='model',
            field=models.CharField(max_length=25),
        ),
        migrations.AlterField(
            model_name='purchaseorder',
            name='indentor',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='vendor',
            name='contact_name',
            field=models.CharField(max_length=45),
        ),
    ]
