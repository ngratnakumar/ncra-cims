from django.db import models
from django.core.validators import MaxValueValidator
from django.utils import timezone

class Vendor(models.Model):
    name = models.CharField(max_length=25)
    contact_name = models.CharField(max_length=45)
    phone1 = models.CharField(max_length=15)
    phone2 = models.CharField(max_length=15)
    phone3 = models.CharField(max_length=15)
    email = models.EmailField()
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name_plural = "Vendors"

    def __str__(self):
        return self.name


class PurchaseOrder(models.Model):
    end_use = models.CharField(max_length=40,default="Ncra Users")
    vendor_name = models.ForeignKey(Vendor, on_delete=models.CASCADE, default=1)
    folder_number = models.CharField(max_length=20,default="NCRA:000L:LTD:2022")
    order_number = models.CharField(max_length=15,default="PO/LO/P0000")
    type = models.CharField(max_length=15,default="Consumable")
    mode = models.CharField(max_length=15,default="Limited Tender")
    enquiry_number = models.CharField(max_length=15,default="ENQ/LO/")
    enquiry_date = models.DateField(default=timezone.now)
    approx_cost = models.CharField(max_length=15,default="0")
    budget = models.CharField(max_length=15)
    delivered_to = models.CharField(max_length=25,default="NCRA Stores")
    enquiry_date = models.DateField(default=timezone.now)
    po_amount = models.CharField(max_length=15,default="0")
    po_date = models.DateField(default=timezone.now)
    order_placed = models.BooleanField(default=False)
    delivery_date = models.DateField(default=timezone.now)
    status = models.CharField(max_length=50, default="none")
    indentor = models.CharField(max_length=50)
    indent_date = models.DateField(default=timezone.now)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name_plural = "POs"

    def __str__(self):
        return self.folder_number+' '+self.order_number



class Disk(models.Model):
#    vendor_name = models.ForeignKey(Vendor, on_delete=models.CASCADE, default=1)
    po = models.ForeignKey(PurchaseOrder, on_delete=models.CASCADE, default=1)
    make = models.CharField(max_length=25)
    model = models.CharField(max_length=25)
    serial_number = models.CharField(max_length=35,default="none")
    type = models.CharField(max_length=65)
    capacity = models.CharField(max_length=15)
    issued_to = models.CharField(max_length=45)
    remarks = models.CharField(max_length=95,default="none")
    purchase_date = models.DateField(default=timezone.now)
    warranty_upto = models.DateField(default=timezone.now)
    issued_date = models.DateField(default=timezone.now)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name_plural = "Disks"
 
    def __str__(self):
        return self.make+'-'+self.type+'-'+self.capacity

class Machine(models.Model):
    anumber = models.CharField(max_length=10)
    vendor_name = models.ForeignKey(Vendor, on_delete=models.CASCADE, default=1)
    po = models.ForeignKey(PurchaseOrder, on_delete=models.CASCADE, default=1)
    make = models.CharField(max_length=20)
    model = models.CharField(max_length=45)
    memory = models.CharField(max_length=15)
    serial_number = models.CharField(max_length=15)
    product_number = models.CharField(max_length=15)
    hostname = models.CharField(max_length=25)
    location = models.CharField(max_length=25)
    monitor = models.BooleanField(default=True)
    keyboard_mouse = models.BooleanField(default=True)
    issued_date = models.DateField(default=timezone.now)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name_plural = "Machines"
 
    def __str__(self):
        return self.anumber+' '+self.make+' '+self.model+' '+self.hostname
