from django.contrib import admin

from django.utils.html import format_html
from django.urls import reverse

from inventory.models import *


def linkify(field_name):
    """
    Converts a foreign key value into clickable links.
    
    If field_name is 'parent', link text will be str(obj.parent)
    Link will be admin url for the admin url for obj.parent.id:change
    """
    def _linkify(obj):
        linked_obj = getattr(obj, field_name)
        if linked_obj is None:
            return '-'
        app_label = linked_obj._meta.app_label
        model_name = linked_obj._meta.model_name
        view_name = f'admin:{app_label}_{model_name}_change'
        link_url = reverse(view_name, args=[linked_obj.pk])
        return format_html('<a href="{}">{}</a>', link_url, linked_obj)

    _linkify.short_description = field_name  # Sets column name
    return _linkify

@admin.register(Vendor)
class RequestDemoAdmin(admin.ModelAdmin):
  list_display = ('name','contact_name','phone1','phone2','phone3','email',)
#  list_display = [field.name for field in Vendor._meta.get_fields()]

@admin.register(PurchaseOrder)
class RequestDemoAdmin(admin.ModelAdmin):
  list_display = ('folder_number','order_number',linkify(field_name="vendor_name"),'type','mode','end_use','budget','delivered_to','enquiry_date','po_date','indentor','order_placed','delivery_date','status',)
 # list_display = [field.name for field in PurchaseOrder._meta.get_fields()]

@admin.register(Disk)
class RequestDemoAdmin(admin.ModelAdmin):
  list_display = ('make','model', linkify(field_name="po"),'type','capacity','serial_number','purchase_date','warranty_upto','issued_date','issued_to','remarks',)
  #list_display = ('make','model', 'po','type','capacity','serial_number','purchase_date','warranty_upto','issued_date','issued_to','remarks',)

@admin.register(Machine)
class RequestDemoAdmin(admin.ModelAdmin):
  list_display = [field.name for field in Machine._meta.get_fields()]

