# Generated by Django 4.1.2 on 2022-11-10 08:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0005_vendor_phone1_vendor_phone2_vendor_phone3'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendor',
            name='phone',
            field=models.CharField(default=0, max_length=15),
            preserve_default=False,
        ),
    ]
