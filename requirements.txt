asgiref>=3.5.2
Django>= 4.1.2
django-material-admin>=1.8.6
pip>=22.3
psycopg-binary>=3.1.4
psycopg2>=2.9.5
setuptools>=59.6.0
sqlparse>=0.4.3
