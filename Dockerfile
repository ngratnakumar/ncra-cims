# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /django_docker/code
COPY requirements.txt /django_docker/code/
RUN pip install -r requirements.txt
COPY . /django_docker/code/
